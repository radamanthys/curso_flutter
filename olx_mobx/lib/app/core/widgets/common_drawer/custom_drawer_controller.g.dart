// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'custom_drawer_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CustomDrawerController on _CustomDrawerControllerBase, Store {
  final _$routerAtom = Atom(name: '_CustomDrawerControllerBase.router');

  @override
  String get router {
    _$routerAtom.context.enforceReadPolicy(_$routerAtom);
    _$routerAtom.reportObserved();
    return super.router;
  }

  @override
  set router(String value) {
    _$routerAtom.context.conditionallyRunInAction(() {
      super.router = value;
      _$routerAtom.reportChanged();
    }, _$routerAtom, name: '${_$routerAtom.name}_set');
  }

  final _$_CustomDrawerControllerBaseActionController =
      ActionController(name: '_CustomDrawerControllerBase');

  @override
  void setRouter(String value) {
    final _$actionInfo =
        _$_CustomDrawerControllerBaseActionController.startAction();
    try {
      return super.setRouter(value);
    } finally {
      _$_CustomDrawerControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = 'router: ${router.toString()}';
    return '{$string}';
  }
}
