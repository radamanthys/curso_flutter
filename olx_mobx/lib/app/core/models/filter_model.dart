enum OrderBy { date, price }

// BIT FLAGS (Pesquisar)
const vendorTypeParticular = 1 << 0;
const vendorTypeProfessional = 1 << 1;

class FilterModel {
  FilterModel({
    this.search,
    this.orderBy = OrderBy.date,
    this.minPrice,
    this.maxPrice,
    this.vendorType = vendorTypeParticular | vendorTypeProfessional,
  });

  String search;

  OrderBy orderBy;
  int minPrice;
  int maxPrice;
  int vendorType;
}
