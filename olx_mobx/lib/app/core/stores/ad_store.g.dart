// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ad_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AdStore on _AdStoreBase, Store {
  final _$_AdStoreBaseActionController = ActionController(name: '_AdStoreBase');

  @override
  void setAd(AdModel values) {
    final _$actionInfo = _$_AdStoreBaseActionController.startAction();
    try {
      return super.setAd(values);
    } finally {
      _$_AdStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = '';
    return '{$string}';
  }
}
