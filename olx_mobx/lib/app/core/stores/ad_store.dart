import 'package:mobx/mobx.dart';

import '../../core/models/ad_model.dart';

part 'ad_store.g.dart';

class AdStore = _AdStoreBase with _$AdStore;

abstract class _AdStoreBase with Store {
  // Inicializar com uma lista vazia
  ObservableList<AdModel> ads = ObservableList.of([]);

  @action
  void setAd(AdModel values) => ads.add(values);
}
