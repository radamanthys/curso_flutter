import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

TransitionType transition() {
  return Platform.isIOS
      ? TransitionType.defaultTransition
      : TransitionType.custom;
}

CustomTransition customTransition() {
  return Platform.isIOS ? null : myCustomTransition;
}

/// Transição semelhante ao do iOS
CustomTransition get myCustomTransition => CustomTransition(
      transitionDuration: Duration(milliseconds: 250),
      transitionBuilder: (context, animation, secondaryAnimation, child) {
        final curve = Curves.easeInOut;
        return SlideTransition(
          transformHitTests: false,
          position: Tween<Offset>(
            begin: Offset(1.0, 0.0),
            end: Offset.zero,
          ).animate(
            CurvedAnimation(parent: animation, curve: curve),
          ),
          child: SlideTransition(
            position: Tween<Offset>(
              begin: Offset.zero,
              end: Offset(-1.0, 0.0),
            ).animate(
                CurvedAnimation(parent: secondaryAnimation, curve: curve)),
            child: child,
          ),
        );
      },
    );
