import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/helpers/validators.dart';
import 'signup_controller.dart';
import 'widgets/field_title.dart';
import 'widgets/password_field.dart';

class SignUpPage extends StatefulWidget {
  final String title;
  const SignUpPage({Key key, this.title = "Cadastrar"}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends ModularState<SignUpPage, SignUpController> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        elevation: 0,
      ),
      body: Form(
        key: _formKey,
        child: Observer(
          builder: (_) {
            return ListView(
              padding: const EdgeInsets.all(16),
              children: <Widget>[
                const FieldTitle(
                  title: 'Apelido',
                  subtitle: 'Como aparecerá em seus anúncios.',
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Exemplo: Ítalo W.',
                  ),
                  validator: (text) {
                    if (text.length < 6) return 'Apelido muito curto';
                    return null;
                  },
                  onSaved: controller.setName,
                  enabled: controller.state != SignUpState.loading,
                ),
                const SizedBox(height: 26),
                const FieldTitle(
                  title: 'E-mail',
                  subtitle: 'Enviaremos um e-mail de confirmação.',
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                  validator: (text) {
                    if (!validEmail(text)) return 'E-mail inválido';
                    return null;
                  },
                  onSaved: controller.setEmail,
                  enabled: controller.state != SignUpState.loading,
                ),
                const SizedBox(height: 26),
                const FieldTitle(
                  title: 'Senha',
                  subtitle: 'Use letras, números e caracteres especiais.',
                ),
                PasswordField(
                  onSaved: controller.setPassword,
                  enabled: controller.state != SignUpState.loading,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 24),
                  height: 50,
                  child: RaisedButton(
                    color: Colors.pink,
                    disabledColor: Colors.pink.withAlpha(150),
                    child: controller.state == SignUpState.loading
                        ? CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white),
                          )
                        : Text(
                            'Cadastre-se',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25),
                    ),
                    onPressed: controller.state != SignUpState.loading
                        ? _signUp
                        : null,
                  ),
                ),
                Divider(color: Colors.grey),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const Text(
                        'Já tem uma conta? ',
                        style: TextStyle(fontSize: 16),
                      ),
                      GestureDetector(
                        onTap: () => Modular.to.pop(),
                        child: Text(
                          'Entrar',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Colors.blue,
                            fontSize: 16,
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  void _signUp() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      controller.signUp();
    }
  }
}
