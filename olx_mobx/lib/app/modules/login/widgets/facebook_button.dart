import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../login_controller.dart';

class FacebookButton extends StatelessWidget {
  final LoginController controller = Modular.get<LoginController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16),
      height: 50,
      child: Observer(
        builder: (_) {
          return RaisedButton(
            color: Color.fromRGBO(58, 86, 152, 1),
            disabledColor: Color.fromRGBO(58, 86, 152, 0.7),
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25),
            ),
            onPressed:
                controller.loggedFacebook ? null : controller.loginWithFacebook,
            child: controller.loggedFacebook
                ? CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white))
                : const Text(
                    'Entrar com Facebook',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
          );
        },
      ),
    );
  }
}
