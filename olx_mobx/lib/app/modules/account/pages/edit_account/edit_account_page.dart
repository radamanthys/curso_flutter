import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'edit_account_controller.dart';
import 'widgets/user_type_widget.dart';

class EditAccountPage extends StatefulWidget {
  final String title;
  const EditAccountPage({Key key, this.title = "Editar Conta"})
      : super(key: key);

  @override
  _EditAccountPageState createState() => _EditAccountPageState();
}

class _EditAccountPageState
    extends ModularState<EditAccountPage, EditAccountController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        elevation: 0,
      ),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: <Widget>[
          UserTypeWidget(
            initialValue: controller.user.userType,
            onSaved: (userType) {},
          ),
          TextFormField(
            initialValue: controller.user.name,
            decoration: _buildDecoration('Nome *'),
            validator: (n) {
              if (n.length < 6) return 'Nome inválido';
              return null;
            },
            onSaved: (n) {},
          ),
          TextFormField(
            initialValue: controller.user.phone,
            decoration: _buildDecoration('Telefone *'),
            keyboardType: TextInputType.phone,
            inputFormatters: [
              WhitelistingTextInputFormatter.digitsOnly,
              TelefoneInputFormatter()
            ],
            validator: (t) {
              if (t.length < 15) return 'Telefone inválido';
              return null;
            },
            onSaved: (t) {},
          ),
          TextFormField(
            obscureText: true,
            decoration: _buildDecoration('Nova senha *'),
            validator: (s) {
              if (s.isEmpty) return null;
              if (s.length < 6) return 'Senha muito curta';
              return null;
            },
            autovalidate: true,
          ),
          TextFormField(
            obscureText: true,
            decoration: _buildDecoration('Confirmar nova senha *'),
            validator: (s) {
              if (s.isEmpty) return null;
              if (s.length < 6) return 'Senha muito curta';
              return null;
            },
            autovalidate: true,
          ),
        ],
      ),
    );
  }

  InputDecoration _buildDecoration(String label) {
    return InputDecoration(
      labelText: label,
      labelStyle: TextStyle(
        fontWeight: FontWeight.w800,
        color: Colors.grey,
        fontSize: 18,
      ),
      contentPadding: const EdgeInsets.fromLTRB(16, 10, 12, 10),
    );
  }
}
