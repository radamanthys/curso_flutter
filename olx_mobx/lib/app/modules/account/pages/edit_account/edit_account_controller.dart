import 'package:mobx/mobx.dart';

import '../../../../core/models/user_model.dart';

part 'edit_account_controller.g.dart';

class EditAccountController = _EditAccountControllerBase
    with _$EditAccountController;

abstract class _EditAccountControllerBase with Store {
  final UserModel user = UserModel();
}
