import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../core/widgets/common_drawer/custom_drawer.dart';
import 'account_controller.dart';

class AccountPage extends StatefulWidget {
  final String title;
  const AccountPage({Key key, this.title = "Account"}) : super(key: key);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends ModularState<AccountPage, AccountController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Minha conta'),
        elevation: 0,
        actions: <Widget>[
          FlatButton(
            child: const Text('Editar'),
            textColor: Colors.white,
            onPressed: () => Modular.to.pushNamed("/account/editAccount"),
          ),
        ],
      ),
      drawer: CustomDrawer(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: 180,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(
              'Ítalo Weyder',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          listTile(title: 'Meus anúncios', onTap: () {}),
          listTile(title: 'Favoritos', onTap: () {}),
        ],
      ),
    );
  }

  Widget listTile({String title, Function onTap}) {
    return ListTile(
      title: Text(
        title,
        style: const TextStyle(
          color: Colors.blue,
          fontWeight: FontWeight.w600,
        ),
      ),
      trailing: const Icon(Icons.keyboard_arrow_right),
      contentPadding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 8,
      ),
      onTap: onTap,
    );
  }
}
