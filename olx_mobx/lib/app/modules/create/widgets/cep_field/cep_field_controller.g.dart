// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cep_field_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CepFieldController on _CepFieldControllerBase, Store {
  final _$stateAtom = Atom(name: '_CepFieldControllerBase.state');

  @override
  CepFieldState get state {
    _$stateAtom.context.enforceReadPolicy(_$stateAtom);
    _$stateAtom.reportObserved();
    return super.state;
  }

  @override
  set state(CepFieldState value) {
    _$stateAtom.context.conditionallyRunInAction(() {
      super.state = value;
      _$stateAtom.reportChanged();
    }, _$stateAtom, name: '${_$stateAtom.name}_set');
  }

  final _$cepAtom = Atom(name: '_CepFieldControllerBase.cep');

  @override
  String get cep {
    _$cepAtom.context.enforceReadPolicy(_$cepAtom);
    _$cepAtom.reportObserved();
    return super.cep;
  }

  @override
  set cep(String value) {
    _$cepAtom.context.conditionallyRunInAction(() {
      super.cep = value;
      _$cepAtom.reportChanged();
    }, _$cepAtom, name: '${_$cepAtom.name}_set');
  }

  final _$addressModelAtom = Atom(name: '_CepFieldControllerBase.addressModel');

  @override
  AddressModel get addressModel {
    _$addressModelAtom.context.enforceReadPolicy(_$addressModelAtom);
    _$addressModelAtom.reportObserved();
    return super.addressModel;
  }

  @override
  set addressModel(AddressModel value) {
    _$addressModelAtom.context.conditionallyRunInAction(() {
      super.addressModel = value;
      _$addressModelAtom.reportChanged();
    }, _$addressModelAtom, name: '${_$addressModelAtom.name}_set');
  }

  final _$_CepFieldControllerBaseActionController =
      ActionController(name: '_CepFieldControllerBase');

  @override
  void setState(CepFieldState value) {
    final _$actionInfo =
        _$_CepFieldControllerBaseActionController.startAction();
    try {
      return super.setState(value);
    } finally {
      _$_CepFieldControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setCep(String value) {
    final _$actionInfo =
        _$_CepFieldControllerBaseActionController.startAction();
    try {
      return super.setCep(value);
    } finally {
      _$_CepFieldControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setAddressModel(AddressModel value) {
    final _$actionInfo =
        _$_CepFieldControllerBaseActionController.startAction();
    try {
      return super.setAddressModel(value);
    } finally {
      _$_CepFieldControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'state: ${state.toString()},cep: ${cep.toString()},addressModel: ${addressModel.toString()}';
    return '{$string}';
  }
}
