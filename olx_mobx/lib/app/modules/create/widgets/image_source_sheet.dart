import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageSourceSheet extends StatelessWidget {
  final Function(File) onImageSelected;

  const ImageSourceSheet(this.onImageSelected);

  @override
  Widget build(BuildContext context) {
    final _picker = ImagePicker();
    return BottomSheet(
      onClosing: () {},
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            FlatButton(
              onPressed: () async {
                final pickedFile =
                    await _picker.getImage(source: ImageSource.camera);
                final image = File(pickedFile.path);
                onImageSelected(image);
              },
              child: const Text('Câmera'),
            ),
            FlatButton(
              onPressed: () async {
                final pickedFile =
                    await _picker.getImage(source: ImageSource.gallery);
                final image = File(pickedFile.path);
                onImageSelected(image);
              },
              child: const Text('Galeria'),
            ),
          ],
        );
      },
    );
  }
}
