import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../core/helpers/format_field.dart';
import '../../core/widgets/common_drawer/custom_drawer.dart';
import 'create_controller.dart';
import 'widgets/cep_field/cep_field.dart';
import 'widgets/hiden_phone_widget.dart';
import 'widgets/images_field.dart';

class CreatePage extends StatefulWidget {
  final String title;
  const CreatePage({Key key, this.title = "Inserir Anúncio"}) : super(key: key);

  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends ModularState<CreatePage, CreateController> {
  //use 'controller' variable to access controller

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: CustomDrawer(),
      body: Form(
        key: _formKey,
        child: Observer(builder: (_) {
          if (controller.state == CreateState.loading) {
            return Center(
              child: Container(
                width: 300,
                height: 300,
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.pink),
                  strokeWidth: 5,
                ),
              ),
            );
          }

          return ListView(
            children: <Widget>[
              ImagesField(
                initialValue: [],
                onSaved: (i) {
                  controller.adModel.images = i;
                },
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Título *',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: Colors.grey,
                    fontSize: 18,
                  ),
                  contentPadding: const EdgeInsets.fromLTRB(16, 10, 12, 10),
                ),
                validator: (text) {
                  if (text.isEmpty) return 'Campo obrigatório';
                  return null;
                },
                onSaved: (t) {
                  controller.adModel.title = t;
                },
              ),
              TextFormField(
                maxLines: null,
                decoration: InputDecoration(
                  labelText: 'Descrição *',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: Colors.grey,
                    fontSize: 18,
                  ),
                  contentPadding: const EdgeInsets.fromLTRB(16, 10, 12, 10),
                ),
                validator: (text) {
                  if (text.trim().isEmpty) return 'Campo obrigatório';
                  if (text.trim().length < 10) return 'Descrição muito curta';
                  return null;
                },
                onSaved: (d) {
                  controller.adModel.description = d;
                },
              ),
              CepField(
                decoration: InputDecoration(
                  labelText: 'CEP *',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: Colors.grey,
                    fontSize: 18,
                  ),
                  contentPadding: const EdgeInsets.fromLTRB(16, 10, 12, 10),
                ),
                onSaved: (a) {
                  controller.adModel.address = a;
                },
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Preço *',
                  prefixText: 'R\$ ',
                  prefixStyle: TextStyle(color: Colors.black, fontSize: 16),
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: Colors.grey,
                    fontSize: 18,
                  ),
                  contentPadding: const EdgeInsets.fromLTRB(16, 10, 12, 10),
                ),
                keyboardType: const TextInputType.numberWithOptions(
                  decimal: true,
                  signed: false,
                ),
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                  RealInputFormatter(centavos: true),
                ],
                validator: (text) {
                  if (text.isEmpty) return 'Campo obrigatório';
                  if (int.tryParse(getSanitizedText(text)) == null) {
                    return 'Utilize valores válidos';
                  }
                  return null;
                },
                onSaved: (p) {
                  controller.adModel.price =
                      int.parse(getSanitizedText(p)) / 100;
                },
              ),
              HidenPhoneWidget(
                initialValue: false,
                onSaved: (h) {
                  controller.adModel.hidePhone = h;
                },
              ),
              Container(
                height: 50,
                child: RaisedButton(
                  color: Colors.pink,
                  child: Text(
                    'Enviar',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w500),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();

                      controller.saveAd();
                    }
                  },
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
