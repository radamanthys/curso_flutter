import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../core/models/ad_model.dart';
import '../../core/stores/ad_store.dart';

part 'create_controller.g.dart';

enum CreateState { idle, loading, done }

class CreateController = _CreateControllerBase with _$CreateController;

abstract class _CreateControllerBase with Store {
  final AdStore adStore;

  _CreateControllerBase(this.adStore);

  AdModel adModel = AdModel();

  @observable
  CreateState state;

  @action
  void setState(CreateState value) => state = value;

  Future<void> saveAd() async {
    setState(CreateState.loading);
    // manda o ad pro repositorio
    await Future.delayed(Duration(seconds: 2));

    adStore.setAd(adModel);

    setState(CreateState.done);
    if (state == CreateState.done) {
      Modular.to.pushReplacementNamed('/');
    }
  }
}
