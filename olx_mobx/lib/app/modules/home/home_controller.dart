import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../core/stores/ad_store.dart';
import 'widgets/search_dialog.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final AdStore adStore;

  _HomeControllerBase(this.adStore);

  @observable
  String search = '';

  @action
  void setSearch(String value) => search = value;

  void openSeach(String currentSearch) async {
    final String search = await Modular.to
        .showDialog(builder: (_) => SearchDialog(currentSearch: currentSearch));

    // final String search = await showDialog(
    //     context: context,
    //     builder: (context) => SearchDialog(currentSearch: currentSearch));

    if (search != null) {
      setSearch(search);
    }
  }
}
