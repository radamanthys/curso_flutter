import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../core/widgets/common_drawer/custom_drawer.dart';
import 'home_controller.dart';
import 'widgets/product_tile.dart';
import 'widgets/top_bar.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: GestureDetector(
          onTap: () => controller.openSeach(controller.search),
          child: LayoutBuilder(
            //! Utilizado para esticar o continer o máximo possível
            builder: (context, constraints) {
              return Container(
                width: constraints.biggest.width,
                child: Observer(
                  builder: (_) {
                    return Text('${controller.search}');
                  },
                ),
              );
            },
          ),
        ),
        actions: <Widget>[
          Observer(
            builder: (_) {
              return controller.search.isEmpty
                  ? IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () => controller.openSeach(''),
                    )
                  : IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () => controller.setSearch(''),
                    );
            },
          ),
        ],
      ),
      drawer: CustomDrawer(),
      body: Column(
        children: <Widget>[
          TopBar(),
          Expanded(
            child: Observer(
              builder: (_) {
                var ads = controller.adStore.ads;
                return ListView.builder(
                  itemCount: ads.length,
                  itemBuilder: (context, index) {
                    return ProductTile(id: index);
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
