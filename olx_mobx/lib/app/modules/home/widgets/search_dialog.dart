import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SearchDialog extends StatefulWidget {
  final String currentSearch;

  const SearchDialog({Key key, this.currentSearch}) : super(key: key);

  @override
  _SearchDialogState createState() => _SearchDialogState(currentSearch);
}

class _SearchDialogState extends State<SearchDialog> {
  final TextEditingController _editingController;

  // O ':' é usado para o iniciar juntamente com o construtor
  // ao inves de iniciar depois
  _SearchDialogState(String currentSearch)
      : _editingController = TextEditingController(text: currentSearch);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
          left: 2,
          right: 2,
          top: 2,
          child: Card(
            child: TextField(
              controller: _editingController,
              textInputAction: TextInputAction.search,
              autofocus: true,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.symmetric(vertical: 15),
                prefixIcon: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.grey[700],
                  ),
                  onPressed: () => Modular.to.pop(),
                ),
                suffixIcon: IconButton(
                  icon: Icon(Icons.close, color: Colors.grey[700]),
                  onPressed: _editingController.clear,
                ),
              ),
              onSubmitted: (text) {
                Modular.to.pop(text);
              },
            ),
          ),
        )
      ],
    );
  }
}
