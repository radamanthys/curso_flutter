import 'package:flutter/material.dart';

import '../../../../../core/models/filter_model.dart';

class VendorTypeField extends StatelessWidget {
  final FormFieldSetter<int> onSaved;
  final int initialValue;

  const VendorTypeField({Key key, this.onSaved, this.initialValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormField<int>(
      initialValue: initialValue,
      onSaved: onSaved,
      builder: (state) {
        return Row(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                // Se o particular está habilitado e for clicado
                if (state.value & vendorTypeParticular != 0) {
                  // E se o professional está habilitado
                  if (state.value & vendorTypeProfessional != 0) {
                    // Desativa a flag particular utilizando o '& ~'
                    state.didChange(state.value & ~vendorTypeParticular);
                  } else {
                    // Habilita o profissional
                    state.didChange(vendorTypeProfessional);
                  }
                } else {
                  // Hbilita flag Particular
                  state.didChange(state.value | vendorTypeParticular);
                }
              },
              child: Container(
                height: 50,
                width: 120,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: state.value & vendorTypeParticular != 0
                            ? Colors.transparent
                            : Colors.grey),
                    borderRadius: const BorderRadius.all(Radius.circular(50)),
                    color: state.value & vendorTypeParticular != 0
                        ? Colors.blue
                        : Colors.transparent),
                alignment: Alignment.center,
                child: Text(
                  'Particular',
                  style: TextStyle(
                      color: state.value & vendorTypeParticular != 0
                          ? Colors.white
                          : Colors.black),
                ),
              ),
            ),
            const SizedBox(width: 10),
            GestureDetector(
              onTap: () {
                if (state.value & vendorTypeProfessional != 0) {
                  if (state.value & vendorTypeParticular != 0) {
                    state.didChange(state.value & ~vendorTypeProfessional);
                  } else {
                    state.didChange(vendorTypeParticular);
                  }
                } else {
                  state.didChange(state.value | vendorTypeProfessional);
                }
              },
              child: Container(
                height: 50,
                width: 120,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: state.value & vendorTypeProfessional != 0
                            ? Colors.transparent
                            : Colors.grey),
                    borderRadius: const BorderRadius.all(Radius.circular(50)),
                    color: state.value & vendorTypeProfessional != 0
                        ? Colors.blue
                        : Colors.transparent),
                alignment: Alignment.center,
                child: Text(
                  'Profissional',
                  style: TextStyle(
                      color: state.value & vendorTypeProfessional != 0
                          ? Colors.white
                          : Colors.black),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
