import 'package:flutter/material.dart';

import '../../../../../core/helpers/format_field.dart';
import '../../../../../core/models/ad_model.dart';

class MainPanel extends StatelessWidget {
  final AdModel adModel;

  const MainPanel(this.adModel);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 24, bottom: 14),
          child: Text(
            'R\$${numToString(adModel.price)}',
            style: TextStyle(
              fontSize: 34,
              letterSpacing: 2.8,
              fontWeight: FontWeight.w300,
              color: Colors.grey[800],
            ),
          ),
        ),
        Text(
          adModel.title,
          style: TextStyle(
            fontSize: 18,
            letterSpacing: 1,
            fontWeight: FontWeight.w400,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            top: 20,
            bottom: 18,
          ),
          child: Text(
            'Publicado em ${dateToString(adModel.dateCreated)}',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: Colors.grey[600],
            ),
          ),
        )
      ],
    );
  }
}
