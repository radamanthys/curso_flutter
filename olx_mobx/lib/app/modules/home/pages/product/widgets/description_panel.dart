import 'package:flutter/material.dart';

import '../../../../../core/models/ad_model.dart';

class DescriptionPanel extends StatefulWidget {
  final AdModel adModel;

  const DescriptionPanel(this.adModel);

  @override
  _DescriptionPanelState createState() => _DescriptionPanelState();
}

class _DescriptionPanelState extends State<DescriptionPanel> {
  AdModel get adModel => widget.adModel;

  bool open = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 18),
          child: Text(
            'Descrição',
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 18),
          child: Text(
            adModel.description,
            maxLines: open ? 10 : 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
          ),
        ),
        if (open || adModel.description.length < 100)
          const SizedBox(height: 18)
        else
          Align(
            alignment: Alignment.centerLeft,
            child: FlatButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                setState(() {
                  open = true;
                });
              },
              child: Text(
                'Ver descrição completa',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  color: Colors.pink,
                ),
              ),
            ),
          )
      ],
    );
  }
}
