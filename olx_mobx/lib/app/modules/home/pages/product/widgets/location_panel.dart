import 'package:flutter/material.dart';

import '../../../../../core/models/ad_model.dart';

class LocationPanel extends StatelessWidget {
  final AdModel adModel;

  LocationPanel(this.adModel);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 18, bottom: 18),
          child: Text(
            'Localização',
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const <Widget>[
                  Text('CEP'),
                  SizedBox(height: 12),
                  Text('Município'),
                  SizedBox(height: 12),
                  Text('Bairro')
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('${adModel.address.cep}'),
                    const SizedBox(height: 12),
                    Text('${adModel.address.localidade}'),
                    const SizedBox(height: 12),
                    Text('${adModel.address.bairro}'),
                  ],
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}
