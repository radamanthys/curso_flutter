import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../core/api/api_error.dart';
import '../core/api/api_response.dart';
import '../core/constant.dart';
import '../core/models/address_model.dart';

class CepRepository extends Disposable {
  final Dio _client;

  CepRepository(this._client);

  Future<ApiResponse> fetchCep(String cep) async {
    var cleanCep = cep.replaceAll('.', '').replaceAll('-', '');

    try {
      final response = await _client.get('$endpoint/$cleanCep/json/');

      // Quando é passado um CEP inválido, retorna um 'erro: true'
      if (response.data.containsKey('erro') && response.data['erro']) {
        // se o error: true
        return ApiResponse.error(
          error: ApiError(
            code: response.statusCode,
            message: 'CEP inválido',
          ),
        );
      }

      final address = AddressModel.fromJson(response.data);

      return ApiResponse.success(result: address);
    } on DioError catch (e) {
      return ApiResponse.error(
        error: ApiError(
            code: -1,
            message: 'Falha ao contactar VIACEP. Exception: ${e.message}'),
      );
    }
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
