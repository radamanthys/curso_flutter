import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buttonMusic(1, Colors.red),
              buttonMusic(2, Colors.orange),
              buttonMusic(3, Colors.yellow),
              buttonMusic(4, Colors.green),
              buttonMusic(5, Colors.teal),
              buttonMusic(6, Colors.blue),
              buttonMusic(7, Colors.deepPurple),
            ],
          ),
        ),
      ),
    );
  }

  Widget buttonMusic(int note, Color color) {
    final player = AudioCache();

    return Expanded(
      child: FlatButton(
        color: color,
        onPressed: () {
          player.play('note$note.wav');
        },
      ),
    );
  }
}
