import 'package:bmi_calculator_flutter/app/app_module.dart';
import 'package:flutter/material.dart';

void main() => runApp(AppModule());
