import 'package:bmi_calculator_flutter/app/shared/theme.dart';
import 'package:flutter/material.dart';
import 'package:bmi_calculator_flutter/app/pages/home/home_module.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Slidy',
      theme: themeDark(context),
      home: HomeModule(),
    );
  }
}
