import 'package:flutter/material.dart';

import 'constants.dart';

ThemeData themeDark(BuildContext context) {
  return ThemeData.dark().copyWith(
    primaryColor: kPrimary_color,
    scaffoldBackgroundColor: kPrimary_color,
    sliderTheme: SliderTheme.of(context).copyWith(
      activeTrackColor: Colors.white,
      inactiveTrackColor: kInactive_color,
      thumbColor: kColorPink,
      overlayColor: Color(0x1fEB1555),
      thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15),
      overlayShape: RoundSliderOverlayShape(overlayRadius: 30),
    ),
  );
}
