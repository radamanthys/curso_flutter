import 'dart:math';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';

class ResultBloc extends BlocBase {
  double _bmi;

  final int height;
  final int weight;


   BehaviorSubject<String> _bmiController; //= BehaviorSubject<String>();
  Stream<String> get outBmi => _bmiController.stream;

  final _resultController = BehaviorSubject<String>();
  Stream<String> get outResult => _resultController.stream;

  final _interpretationController = BehaviorSubject<String>();
  Stream<String> get outInterpretation => _interpretationController.stream;

  ResultBloc({this.height, this.weight}) {
    _bmiController = BehaviorSubject<String>();


//    print('Altura: $height \nPeso: $weight');
    getResult();
  }


  void getResult() {
    print('Altura: $height \nPeso: $weight');
    // Calcular BMI
    _bmi = weight / pow(height / 100, 2);
    _bmiController.add(_bmi.toStringAsFixed(1));

    if (_bmi >= 25) {
      _resultController.add('Overweight');
      _interpretationController.add('You hame a height than body weight. Try to exercise more.');
    } else if (_bmi > 18.5) {
      _resultController.add('Normal');
      _interpretationController.add('You have a normal body weight. God job');
    } else {
      _resultController.add('Underwight');
      _interpretationController.add('You have a lower normal body weight. You can eat a bit more.');
    }
  }

  //dispose will be called automatically by closing its streams
  @override
  void dispose() {
    _bmiController.close();
    _resultController.close();
    _interpretationController.close();
    super.dispose();
  }
}
