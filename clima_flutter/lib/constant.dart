import 'package:flutter/material.dart';

const String kBaseUrl = 'https://api.openweathermap.org/data/2.5/';
const String apiKey = 'b3f2b7c3d4d2d94e79059955b698fd28';

const kTempTextStyle = TextStyle(
  fontFamily: 'Spartan MB',
  fontSize: 60.0,
);

const kMessageTextStyle = TextStyle(
  fontFamily: 'Spartan MB',
  fontSize: 60.0,
);

const kButtonTextStyle = TextStyle(
  fontSize: 30.0,
  fontFamily: 'Spartan MB',
);

const kConditionTextStyle = TextStyle(
  fontSize: 100.0,
);
