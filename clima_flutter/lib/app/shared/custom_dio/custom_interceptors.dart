import 'package:clima_flutter/constant.dart';
import 'package:dio/dio.dart';

class CustomInterceptors extends InterceptorsWrapper {
  @override
  onRequest(RequestOptions options) async {
    // * Adicionando accessToken no header
    // options.headers = {HttpHeaders.authorizationHeader: "Bearer $_token"};
    options.queryParameters = {'appid': apiKey, 'units': 'metric'};
    
    print("REQUEST [${options.method}] => PATH: ${options.path}");
    return options;
  }

  @override
  onResponse(Response response) async {
    //200
    //201
    print(
        "RESPONSE [${response.statusCode}] => PATH: ${response.request.path}");
    return response;
  }

  @override
  onError(DioError e) async {
    //Exception
    print("ERROR [${e.response.statusCode}] => PATH: ${e.request.path}");
    if (e.response.statusCode == 404) return DioError(error: "Erro interno");

    return e;
  }
}
