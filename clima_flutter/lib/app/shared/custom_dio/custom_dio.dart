import 'package:clima_flutter/app/shared/custom_dio/custom_interceptors.dart';
import 'package:clima_flutter/constant.dart';
import 'package:dio/dio.dart';

class CustomDio {

  final Dio client;

  CustomDio(this.client) {
    client.options.baseUrl = kBaseUrl;
    client.options.connectTimeout = 5000;
    client.options.receiveTimeout = 3000;
    client.interceptors.add(CustomInterceptors());
  }
}
