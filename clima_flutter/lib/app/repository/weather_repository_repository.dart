import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:clima_flutter/app/shared/custom_dio/custom_dio.dart';
import 'package:clima_flutter/app/shared/models/weather_coord_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class WeatherRepositoryRepository extends Disposable {
  final CustomDio _dio;

  WeatherRepositoryRepository(this._dio);

  Future<WeatherCoordModel> getDataCoordinates(
      {@required double latitude, @required double longitude}) async {
    try {
      Response response =
          await _dio.client.get('weather?lat=$latitude&lon=$longitude');

          // print(response);

      return WeatherCoordModel.fromJson(response.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
