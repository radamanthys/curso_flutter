import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:clima_flutter/app/shared/models/weather.dart';
import 'package:clima_flutter/app/shared/models/weather_coord_model.dart';
import 'package:rxdart/rxdart.dart';

class LocationBloc extends BlocBase {
  final WeatherCoordModel weatherData;
  WeatherModel weatherModel = WeatherModel();

  String weatherIcon;
  String message;

  BehaviorSubject<WeatherCoordModel> _weatherController;
  Stream<WeatherCoordModel> get outWeather => _weatherController.stream;

  LocationBloc({this.weatherData}) {
    _weatherController = BehaviorSubject<WeatherCoordModel>.seeded(weatherData);

    getCondition();
  }

  void getCondition() {
    _weatherController.listen((data) {
      weatherIcon = weatherModel.getWeatherIcon(data.weather[0].id);
      message = weatherModel.getMessage(data.main.temp.toInt());

      print(data.weather[0].id);
      print(data.main.temp.toInt());
    });
  }

  //dispose will be called automatically by closing its streams
  @override
  void dispose() {
    _weatherController.close();
    // _conditionController.close();
    super.dispose();
  }
}
