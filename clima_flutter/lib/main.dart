import 'package:flutter/material.dart';
import 'package:clima_flutter/app/app_module.dart';

void main() => runApp(AppModule());
